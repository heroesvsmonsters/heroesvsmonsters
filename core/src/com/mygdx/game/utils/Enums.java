package com.mygdx.game.utils;

public class Enums {
    public enum GameType {
        SINGLEPLAYER,
        MULTIPLAYER,
        MENU
    }

    public enum SettingsBackground {
        MENU,
        CITY,
    }

    public enum SliderType {
        AUDIO,
        SFX
    }

    public enum Trophy {
        GOLD,
        SILVER,
        BRONZE
    }
}
